<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="lengow_order")
 */
class LengowOrder {

    /**
     * @ORM\Column(type="string", length=100)
     * @ORM\Id
     */
    private $order_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $marketplace;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $order_amount;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $order_currency;

    /**
     * Set order_id
     *
     * @param integer $orderId
     * @return Order
     */
    public function setOrderId($orderId) {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * Get order_id
     *
     * @return integer 
     */
    public function getOrderId() {
        return $this->order_id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Order
     */
    public function setMarketplace($marketplace) {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace() {
        return $this->marketplace;
    }

    /**
     * Set order_amount
     *
     * @param string $orderAmount
     * @return Order
     */
    public function setOrderAmount($orderAmount) {
        $this->order_amount = $orderAmount;

        return $this;
    }

    /**
     * Get order_amount
     *
     * @return string 
     */
    public function getOrderAmount() {
        return $this->order_amount;
    }

    /**
     * Set order_currency
     *
     * @param string $orderCurrency
     * @return Order
     */
    public function setOrderCurrency($orderCurrency) {
        $this->order_currency = $orderCurrency;

        return $this;
    }

    /**
     * Get order_currency
     *
     * @return string 
     */
    public function getOrderCurrency() {
        return $this->order_currency;
    }

}
