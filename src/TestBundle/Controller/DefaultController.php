<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Column\ActionsColumn;
use APY\DataGridBundle\Grid\Action\RowAction;

class DefaultController extends Controller {

    /**
     * Create and display grid used on main page
     * @return type
     */
    public function indexAction() {
        // Check if new orders
        $this->get('lengow_test')->loadOrders();

        $source = new Entity('TestBundle:LengowOrder');

        $grid = $this->container->get('grid');

        $grid->setSource($source);

        // Add column to export data
        $jsonExport = new RowAction('Json', '_api');
        $yamlExport = new RowAction('Yaml', '_api');
        $exportColumn = new ActionsColumn(null, 'Export', array($jsonExport, $yamlExport));

        $grid->addColumn($exportColumn);

        return $grid->getGridResponse('TestBundle:LengowOrder:index.html.twig');
    }

}
