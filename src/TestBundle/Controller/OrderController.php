<?php

namespace TestBundle\Controller;

use TestBundle\Entity\LengowOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrderController extends Controller {

    private $url_orders;
    private $logger;

    /**
     * Constructor
     * @param type $url_orders Url to the XML file
     * @param type $logger Logger
     */
    public function __construct($url_orders, $logger) {
        $this->url_orders = $url_orders;
        $this->logger = $logger;
    }

    /**
     * Read distant XML file and register each order
     */
    public function loadOrders() {
        $this->logger->info('Downloading xml file');

        $xml = simplexml_load_string(file_get_contents($this->url_orders));

        $this->logger->info('Reading xml file');

        // For each order found
        foreach ($xml->orders->order as $order) {
            $this->registerOrder(
                    $order->order_id, $order->marketplace, $order->order_amount, $order->order_currency
            );
        }
    }


    /**
     * Register an order in the database
     * @param type $id Order id
     * @param type $marketplace Order marketplace
     * @param type $amount Order amount
     * @param type $currency Order currency
     */
    public function registerOrder($id, $marketplace, $amount, $currency) {
        $em = $this->getDoctrine()->getManager();

        $order = $em->getRepository('TestBundle:LengowOrder')
                ->find($id);

        // If the order isn't in the database
        if (!$order) {
            $order = new LengowOrder();
            $order->setOrderId($id);
            $order->setMarketPlace($marketplace);
            $order->setOrderAmount($amount);
            $order->setOrderCurrency($currency);

            $em->persist($order);
            $em->flush();

            $this->logger->info('Order #' . $id . ' stored in database');
        } else {
            $this->logger->notice('Order #' . $id . ' already in database');
        }
    }

}
