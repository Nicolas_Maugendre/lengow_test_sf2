<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

class ExportController extends Controller {

    /**
     * Export orders to json or yaml
     * @param type $export_type Choosen type to export datas
     * @param type $order_id Order id, null to export all orders
     * @return Response
     */
    public function getOrderAction($export_type, $order_id) {
        $em = $this->getDoctrine()->getEntityManager();

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $orders = null;

        // If an order id is specified
        if ($order_id !== null) {
            // Get selected order
            $orders = $em->getRepository('TestBundle:LengowOrder')->find($order_id);
        } else {
            // Find all orders
            $orders = $em->getRepository('TestBundle:LengowOrder')->findAll();
        }

        // Serialize to json
        $jsonContent = $serializer->serialize($orders, 'json');

        // If yaml format selected
        if (strcmp($export_type, 'yaml') === 0) {
            $data = json_decode($jsonContent, true);
            $yml = Yaml::dump($data);
            return new Response($yml);
        } else {
            // Default json format
            return new Response($jsonContent);
        }
    }

}
