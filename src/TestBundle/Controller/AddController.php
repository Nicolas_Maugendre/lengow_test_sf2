<?php

namespace TestBundle\Controller;

use TestBundle\Entity\LengowOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AddController extends Controller {

    /**
     * Create a form to add a new order
     * @param Request $request
     * @return type
     */
    public function addOrderAction(Request $request) {
        $order = new LengowOrder();

        $formBuilder = $this->get('form.factory')->createBuilder('form', $order);

        $form = $formBuilder
                ->add('order_id', 'text')
                ->add('marketplace', 'text')
                ->add('order_amount', 'number')
                ->add('order_currency', 'text')
                ->add('save', 'submit', array('label' => 'Save', 'attr' => array('class' => "btn btn-default")))
                ->getForm()
        ;

        $form->handleRequest($request);

        // If all fields are correct
        if ($form->isValid()) {
            // Add new product to database
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            return $this->redirectToRoute('_homepage');
        }

        // Create view
        return $this->render('TestBundle:LengowOrder:add.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
